import boto3
import schedule

ec2_client = boto3.client('ec2')


def check_instance_status():
    statuses = ec2_client.describe_instance_status(IncludeAllInstances=True)
    try:
        for status in statuses['InstanceStatuses']:
            ins_status = status['InstanceStatus']['Status']
            sys_status = status['SystemStatus']['Status']
            state_status = status['InstanceState']['Name']
            print(f"Instance {status['InstanceId']} {state_status} {ins_status} {sys_status}")
    except Exception as e:
        print(f'Error: {str(e)}')

schedule.every(5).seconds.do(check_instance_status)

while True:
    schedule.run_pending()

