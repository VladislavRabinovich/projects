import boto3

ec2_client_east = boto3.client('ec2', region_name="us-east-1")
ec2_resource_east = boto3.resource('ec2', region_name="us-east-1")

ec2_client_west = boto3.client('ec2', region_name="us-west-2")
ec2_resource_west = boto3.resource('ec2', region_name="us-west-2")

instance_ids_east = []
instance_ids_west = []

reservations_east = ec2_client_east.describe_instances()['Reservations']
for res in reservations_east:
    instances = res['Instances']
    for ins in instances:
        instance_ids_east.append(ins['InstanceId'])


response = ec2_resource_east.create_tags(
    Resources=instance_ids_east,
    Tags=[
        {
            'Key': 'environment',
            'Value': 'prod'
        },
    ]
)

reservations_west = ec2_client_west.describe_instances()['Reservations']
for res in reservations_west:
    instances = res['Instances']
    for ins in instances:
        instance_ids_west.append(ins['InstanceId'])


response = ec2_resource_west.create_tags(
    Resources=instance_ids_west,
    Tags=[
        {
            'Key': 'environment',
            'Value': 'dev'
        },
    ]
)
