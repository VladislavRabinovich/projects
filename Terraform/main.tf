
provider "aws" {
  region = "us-east-1"
  access_key = var.access
  secret_key = var.secret
}

module "network_setup" {
  source = "./modules/network"
  vpc_cidr_block = var.vpc_cidr_block
  subnet_cidr_block = var.subnet_cidr_block
  zone = var.zone
}

resource "aws_key_pair" "ssh-key" {
  key_name   = "aws_key"
  public_key = var.connection_key
}

resource "aws_instance" "new-server" {
  ami           = var.ami_id
  instance_type = var.ec2_type

  subnet_id              = module.network_setup.subnet.id
  vpc_security_group_ids = [module.network_setup.security.id]
  availability_zone      = var.zone

  associate_public_ip_address = true
  key_name                    = "aws_key"

/*  connection {
    type        = "ssh"
    host        = self.public_ip
    user        = "ec2-user"
    private_key = file(var.private_key)
  }
  */

  tags = {
    Name : "test-server"
  }
}

resource "null_resource" "docker-install" {
  triggers = {
    trigger = aws_instance.new-server.public_ip
  }

  provisioner "local-exec" {
    working_dir = var.current_dir
#    command = "ansible-playbook --inventory ${aws_instance.new-server.public_ip}, --private-key ${var.private_key} --user ec2-user ansible/ec2-docker.yaml"
     command = "ansible-playbook ec2-docker.yaml"
  }
}

resource "null_resource" "jenkins-install" {
  triggers = {
    trigger = aws_instance.new-server.public_ip
  }

  provisioner "local-exec" {
    working_dir = var.current_dir
    command     = "ansible-playbook ec2-jenkins.yaml"
  }

  depends_on = [
    null_resource.docker-install
  ]
}

output "new-server-ip" {
  value = aws_instance.new-server.public_ip
}

