resource "aws_vpc" "new-vpc" {
  cidr_block = var.vpc_cidr_block
  enable_dns_hostnames = true
}

resource "aws_subnet" "new-subnet" {
  vpc_id = aws_vpc.new-vpc.id
  cidr_block = var.subnet_cidr_block
  availability_zone = var.zone
}

resource "aws_internet_gateway" "new-igw" {
  vpc_id = aws_vpc.new-vpc.id
}

resource "aws_default_route_table" "main-route" {
  default_route_table_id = aws_vpc.new-vpc.default_route_table_id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.new-igw.id
  }
}

resource "aws_default_security_group" "sec_group" {
  vpc_id = aws_vpc.new-vpc.id

  ingress {
    from_port = 22
    protocol  = "tcp"
    to_port   = 22
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    from_port = 8080
    protocol  = "tcp"
    to_port   = 8085
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port = 0
    protocol  = "-1"
    to_port   = 0
    cidr_blocks = ["0.0.0.0/0"]
    prefix_list_ids = []
  }
}

