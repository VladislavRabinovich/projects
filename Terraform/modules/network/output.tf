output "subnet" {
  value = aws_subnet.new-subnet
}

output "security" {
  value = aws_default_security_group.sec_group
}